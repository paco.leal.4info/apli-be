@extends('layouts.app')

@section('content')
{{ Form::model($inscripcion, ['route' => ['inscripcion.update', $inscripcion], 'method' => 'PUT', 'files' => false]) }}

	<div class="form-group">
	  {{ Form::label('estado', 'Estado') }}
	  {{ Form::select('estado',$estado,null, ['class' => 'form-control', 'id' => 'estado']) }}
	</div>

<div>
  {{ Form::submit('Actualizar', ['class' => 'btn btn-primary btn-block']) }}
</div>
{{ Form::close() }}
@endsection