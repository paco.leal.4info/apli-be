 <ul class="navbar-nav mr-auto links">
      <li class="nav-item {{ in_array('home', explode('.', Route::currentRouteName())) ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('home') }}">Home <span class="sr-only"></span></a>
      </li>
      <li class="nav-item {{ ((in_array('user', explode('.', Route::currentRouteName()))) || (in_array('alumno', explode('.', Route::currentRouteName()))) ||  (in_array('empresa', explode('.', Route::currentRouteName())))) ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('user.index') }}">Usuarios <span class="sr-only"></span></a>
      </li>
      <li class="nav-item {{ in_array('centro', explode('.', Route::currentRouteName())) ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('centro.index') }}">Centros <span class="sr-only"></span></a>
      </li>
      <li class="nav-item {{ in_array('contacto', explode('.', Route::currentRouteName())) ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('contacto.index') }}">Contactos <span class="sr-only"></span></a>
      </li>
      <li class="nav-item {{ in_array('oferta', explode('.', Route::currentRouteName())) ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('oferta.index') }}">Ofertas <span class="sr-only"></span></a>
      </li>
      <li class="nav-item {{ in_array('inscripcion', explode('.', Route::currentRouteName())) ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('inscripcion.index') }}">Inscripciones <span class="sr-only"></span></a>
      </li>
    </ul>