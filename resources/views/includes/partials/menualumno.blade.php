 <ul class="navbar-nav mr-auto links">
      <li class="nav-item {{ in_array('home', explode('.', Route::currentRouteName())) ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('home') }}">Home <span class="sr-only"></span></a>
      </li>
      <li class="nav-item {{ in_array('oferta', explode('.', Route::currentRouteName())) ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('oferta.index') }}">Ofertas <span class="sr-only"></span></a>
      </li>
      <li class="nav-item {{ in_array('inscripcion', explode('.', Route::currentRouteName())) ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('inscripcion.index') }}">Mis Inscripciones <span class="sr-only"></span></a>
      </li>
</ul>