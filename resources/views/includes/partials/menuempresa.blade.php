 <ul class="navbar-nav mr-auto links">
      <li class="nav-item {{ in_array('home', explode('.', Route::currentRouteName())) ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('home') }}">Home <span class="sr-only"></span></a>
      </li>
      <li class="nav-item {{ in_array('alumno', explode('.', Route::currentRouteName())) ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('alumno.index') }}">Alumnos <span class="sr-only"></span></a>
      </li>
      <li class="nav-item {{ in_array('centro', explode('.', Route::currentRouteName())) ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('centro.index') }}">Mis Centros <span class="sr-only"></span></a>
      </li>
      <li class="nav-item {{ in_array('contacto', explode('.', Route::currentRouteName())) ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('contacto.index') }}">Mis Contactos <span class="sr-only"></span></a>
      </li>
      <li class="nav-item {{ in_array('oferta', explode('.', Route::currentRouteName())) ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('oferta.index') }}">Mis Ofertas <span class="sr-only"></span></a>
      </li>
      <li class="nav-item {{ in_array('inscripcion', explode('.', Route::currentRouteName())) ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('inscripcion.index') }}">Mis Inscripciones <span class="sr-only"></span></a>
      </li>
    </ul>