@extends('layouts.app')

@section('content')
{{ Form::model($empresa, ['route' => ['empresa.update', $empresa['id']], 'method' => 'PUT', 'files' => false]) }}

<div class="form-group">
  {{ Form::label('nombre_s', 'Nombre Social') }}
  {{ Form::text('nombre_s',null, ['class' => 'form-control', 'id' => 'nombre_s', 'required']) }}
</div>

<div class="form-group">
  {{ Form::label('id_fiscal', 'CIF / NIF') }}
  {{ Form::text('id_fiscal',null, ['class' => 'form-control', 'id' => 'id_fiscal', 'pattern' => '(?:)(^\d{8}[A-Z]$)|(^([ABCDEFGHJKLMNPQRSUVW])(\d{7})([0-9A-J])$)|(^[XYZ]\d{7,8}[A-Z]$)', 'required']) }}
</div>

<div class="form-group">
  {{ Form::label('dir_social', 'Dirección Social') }}
  {{ Form::text('dir_social',null, ['class' => 'form-control', 'id' => 'dir_social', 'required']) }}
</div>

<div class="form-group">
  {{ Form::label('actividad', 'Actividad Comercial') }}
  {{ Form::select('actividad', ['actividad1','actividad2','actividad3','etc'],null, ['class' => 'form-control', 'id' => 'actividad']) }}
</div>

<div class="form-group">
  {{ Form::label('cnae_iae', 'CNAE / IAE') }}
  {{ Form::text('cnae_iae',null, ['class' => 'form-control', 'id' => 'cnae_iae', 'required']) }}
</div>

<div class="form-group">
  {{ Form::label('web', 'Web') }}
  {{ Form::text('web',null, ['class' => 'form-control', 'id' => 'web', 'required']) }}
</div>

<div class="form-group">
  {{ Form::label('tlf', 'Teléfono') }}
  {{ Form::text('tlf',null, ['class' => 'form-control', 'id' => 'tlf', 'pattern' => '^[69][0-9]{8}', 'placeholder' => '9######## o 6########']) }}
</div>

<div class="form-group">
  {{ Form::label('n_empleados', 'Nº Empleados') }}
  {{ Form::number('n_empleados',null, ['class' => 'form-control', 'id' => 'n_empleados', 'min' => 1, 'required']) }}
</div>


<div>
  {{ Form::submit('Actualizar', ['class' => 'btn btn-primary btn-block']) }}
</div>
{{ Form::close() }}
@endsection