@extends('layouts.app')

@section('content')
<h2>Empresa</h2>
<div class="list-group">
  <a href="#" class="list-group-item list-group-item-action disabled">Nombre Comercial: {{ $empresa->user['name'] }}</a>

  <a href="#" class="list-group-item list-group-item-action disabled">Nombre Social: {{ $empresa->nombre_s }}</a>

  <a href="#" class="list-group-item list-group-item-action disabled">CIF / NIF: {{ $empresa->id_fiscal ? $empresa->id_fiscal : 'No disponible' }}</a>

  <a href="#" class="list-group-item list-group-item-action disabled">Dirección Social: {{ $empresa->dir_social ? $empresa->dir_social : 'No disponible' }}</a>

  <a href="#" class="list-group-item list-group-item-action disabled">Actividad Comercial: {{ $empresa->actividad ? $empresa->actividad : 'No disponible' }}</a>

  <a href="#" class="list-group-item list-group-item-action disabled">CNAE / IAE: {{ $empresa->cnae_iae ? $empresa->cnae_iae : 'No disponible' }}</a>

  <a href="#" class="list-group-item list-group-item-action disabled">Web:
    {{ $empresa->web ? $empresa->web : 'No disponible' }}
</a>

  <a href="#" class="list-group-item list-group-item-action disabled">Teléfono: {{ $empresa->tlf ? $empresa->tlf : 'No disponible' }}</a>

  <a href="#" class="list-group-item list-group-item-action disabled">Nº Empleados: {{ $empresa->n_empleados ? $empresa->n_empleados : 'No disponible' }}</a>
</div>

  @if ($empresa->id == Auth::user()->empresa['id'])
  	<a href="{{ route('empresa.edit', $empresa->id) }}" class="btn btn-success btn-block">Editar</a>
  @endif

<a class="btn btn-secondary btn-block mt-4" href="{{ URL::previous() }}">Volver</a>
@endsection