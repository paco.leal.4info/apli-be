@extends('layouts.app')

@section('content')
<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">
                <span>{{ __('Regístrate') }}</span>
            </div>
            <div class="card-body">
                <form method="POST" action="{{ route('register') }}">
                    @csrf

                    @include('auth.form')

                    <div class="form-group row mb-0" style="margin-top: 2em;">
                        <div class="col-md-6 offset-md-5">
                            <button type="submit" class="btn btn-primary">
                                {{ __('Registrarse') }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
function toggle() {
    var formalumno = document.getElementById('formalumno');
    var formempresa = document.getElementById('formempresa');
    if (formalumno.style.display == "none") {
        formalumno.style.display = "block";
        formempresa.style.display = "none";
    } else {
        formalumno.style.display = "none";
        formempresa.style.display = "block";
    }
};
</script>
@endsection
