@extends('layouts.app')

@section('content')
<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">{{ __('Verifica tu Dirección de Email') }}</div>

            <div class="card-body">
                @if (session('resent'))
                    <div class="alert alert-success" role="alert">
                        {{ __('Se ha enviado un enlace de verificación a tu dirección de email.') }}
                    </div>
                @endif

                {{ __('Antes de continuar, por favor revisa el enlace de verificación en tu email.') }}
                {{ __('Si no has recibido el correo') }}, <a href="{{ route('verification.resend') }}">{{ __('haz click aquí para recibir otro') }}</a>.
            </div>
        </div>
    </div>
</div>
@endsection
