@csrf
<!--
Parte general
-->
<div class="form-group row">
    <label id="name" for="name" class="col-md-5 col-form-label text-md-right">{{ __('Nombre Completo / Comercial') }}</label>

    <div class="col-md-6">
        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

        @error('name')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="email" class="col-md-5 col-form-label text-md-right">{{ __('E-Mail') }}</label>

    <div class="col-md-6">
        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

        @error('email')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="password" class="col-md-5 col-form-label text-md-right">{{ __('Contraseña') }}</label>

    <div class="col-md-6">
        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

        @error('password')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="password-confirm" class="col-md-5 col-form-label text-md-right">{{ __('Confirma la Contraseña') }}</label>

    <div class="col-md-6">
        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
    </div>
</div>
<div class="form-group row">
  <label for="formselect" class="col-md-5 col-form-label text-md-right">¿Cómo quieres darte de alta?</label>
  <div class="col-md-6">
      <select class="form-control" id="formselect" name="formselect" onchange="toggle()">
        <option value="0" selected>Como Alumno</option>
        <option value="1">Como Empresa</option>
        @if (Auth::user() && Auth::user()->codigo)
        	<option value="2">Como Profesor</option>
        @endif
      </select>
  </div>
</div>

<!--
Parte para alumnos
-->
<div id="formalumno">
    <hr>
    <div class="form-group row">
        <label id="fec_nac" for="fec_nac" class="col-md-5 col-form-label text-md-right">{{ __('Fecha de Nacimiento *') }}</label>

        <div class="col-md-6">
            <input id="fec_nac" type="date"  min="1920-12-31" class="form-control @error('fec_nac') is-invalid @enderror" name="fec_nac" value="{{ old('fec_nac') }}" autocomplete="fec_nac" autofocus>

            @error('fec_nac')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>

    <div class="form-group row">
        <label for="tlf" class="col-md-5 col-form-label text-md-right">{{ __('Teléfono') }}</label>

        <div class="col-md-6">
            <input id="tlf" type="tel" class="form-control @error('tlf') is-invalid @enderror" name="tlf" value="{{ old('tlf') }}" autocomplete="tlf" placeholder="9######## o 6########" pattern="^[69][0-9]{8}">

            @error('tlf')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>

    <div class="form-group row">
        <label for="cp" class="col-md-5 col-form-label text-md-right">{{ __('Código Postal') }}</label>

        <div class="col-md-6">
            <input id="cp" type="text" class="form-control @error('cp') is-invalid @enderror" name="cp" value="{{ old('cp') }}" autocomplete="cp" placeholder="#####" pattern="[0-9]{5}">

            @error('cp')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
</div>


<!--
Parte para empresas
-->
<div id="formempresa" style="display:none;">
    <hr>
    <div class="form-group row">
        <label id="nombre_s" for="nombre_s" class="col-md-5 col-form-label text-md-right">{{ __('Razón Social') }}</label>

        <div class="col-md-6">
            <input id="nombre_s" type="text" class="form-control @error('nombre_s') is-invalid @enderror" name="nombre_s" value="{{ old('nombre_s') }}" autocomplete="nombre_s" autofocus>

            @error('nombre_s')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>

    <div class="form-group row">
        <label id="id_fiscal" for="id_fiscal" class="col-md-5 col-form-label text-md-right">{{ __('NIF / CIF') }}</label>

        <div class="col-md-6">
            <input id="id_fiscal" type="text" class="form-control @error('id_fiscal') is-invalid @enderror" name="id_fiscal" value="{{ old('id_fiscal') }}" autocomplete="id_fiscal" autofocus placeholder="12345678Z" pattern="(?:)(^\d{8}[A-Z]$)|(^([ABCDEFGHJKLMNPQRSUVW])(\d{7})([0-9A-J])$)|(^[XYZ]\d{7,8}[A-Z]$)">

            @error('id_fiscal')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>

    <div class="form-group row">
        <label id="dir_social" for="dir_social" class="col-md-5 col-form-label text-md-right">{{ __('Dirección Social') }}</label>

        <div class="col-md-6">
            <input id="dir_social" type="text" class="form-control @error('dir_social') is-invalid @enderror" name="dir_social" value="{{ old('dir_social') }}" autocomplete="dir_social" autofocus>

            @error('dir_social')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>

    <div class="form-group row">
      <label for="actividad" class="col-md-5 col-form-label text-md-right">Actividad Comercial</label>
      <div class="col-md-6">
          <select class="form-control" id="actividad" name="actividad">
            <option value="actividad1">Opción 1</option>
            <option value="actividad2">Opción 2</option>
            <option value="actividad3">Opción 3</option>
            <option value="etc">Otros</option>
          </select>
      </div>
    </div>

    <div class="form-group row">
        <label id="cnae_iae" for="cnae_iae" class="col-md-5 col-form-label text-md-right">{{ __('CNAE / IAE') }}</label>

        <div class="col-md-6">
            <input id="cnae_iae" type="text" class="form-control @error('cnae_iae') is-invalid @enderror" name="cnae_iae" value="{{ old('cnae_iae') }}" autocomplete="cnae_iae" autofocus>

            @error('cnae_iae')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
</div>

@if (Auth::user() && Auth::user()->codigo)
	<div id="formprofesor" style="display:none;">
		<div class="form-group row">
		    <label for="codigo" class="col-md-5 col-form-label text-md-right">{{ __('Código') }}</label>

		    <div class="col-md-6">
		        <input id="codigo" type="password" class="form-control @error('codigo') is-invalid @enderror" name="codigo" autocomplete="new-codigo">

		        @error('codigo')
		            <span class="invalid-feedback" role="alert">
		                <strong>{{ $message }}</strong>
		            </span>
		        @enderror
		    </div>
		</div>
	</div>
@endif