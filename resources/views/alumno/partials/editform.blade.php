<form method="POST" action="{{ route('alumno.update', $alumno) }}" enctype="multipart/form-data">
    {{ method_field('PATCH') }}
    @csrf

    <div class="form-group row">
        <label id="name" for="name" class="col-md-5 col-form-label text-md-right">{{ __('Nombre Completo') }}</label>

        <div class="col-md-6">
            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $alumno->user['name'] }}" required autocomplete="name" autofocus>

            @error('name')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>

    <div class="form-group row">
        <label for="email" class="col-md-5 col-form-label text-md-right">{{ __('E-Mail') }}</label>

        <div class="col-md-6">
            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $alumno->user['email'] }}" required autocomplete="email">

            @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div id="formalumno">
        <div class="form-group row">
            <label id="fec_nac" for="fec_nac" class="col-md-5 col-form-label text-md-right">{{ __('Fecha de Nacimiento *') }}</label>

            <div class="col-md-6">
                <input id="fec_nac" type="date"  min="1920-12-31" class="form-control @error('fec_nac') is-invalid @enderror" name="fec_nac" value="{{ $alumno->fec_nac }}" autocomplete="fec_nac" autofocus required>

                @error('fec_nac')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>

        <div class="form-group row">
            <label for="tlf" class="col-md-5 col-form-label text-md-right">{{ __('Teléfono') }}</label>

            <div class="col-md-6">
                <input id="tlf" type="tel" class="form-control @error('tlf') is-invalid @enderror" name="tlf" value="{{ $alumno->tlf }}" autocomplete="tlf" placeholder="9######## o 6########" pattern="^[69][0-9]{8}">

                @error('tlf')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>

        <div class="form-group row">
            <label for="cp" class="col-md-5 col-form-label text-md-right">{{ __('Código Postal') }}</label>

            <div class="col-md-6">
                <input id="cp" type="text" class="form-control @error('cp') is-invalid @enderror" name="cp" value="{{ $alumno->cp }}" autocomplete="cp" placeholder="#####" pattern="[0-9]{5}">

                @error('cp')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>
    </div>

    <div class="form-group row">
      <label for="titulacion" class="col-md-5 col-form-label text-md-right">Titulación</label>
      <div class="col-md-6">
          <select class="form-control" id="titulacion" name="titulacion">
            @foreach ($titulaciones as $titulacion)
                <option {{ $titulacion == $alumno->titulacion ? 'selected' : '' }}>{{ $titulacion }}</option>
            @endforeach
          </select>
      </div>
    </div>

    <div class="form-group row">
        <label for="cv" class="col-md-5 col-form-label text-md-right">Currículum Vitae</label>
        <div class="input-group col-md-6">
            <input type="file" name="cv" class="form-control-file btn text-left">
        </div>
    </div>

    <div class="form-group row">
      <label for="estado_cv" class="col-md-5 col-form-label text-md-right">Estado CV</label>
      <div class="col-md-6">
          <select class="form-control" id="estado_cv" name="estado_cv">
            <option {{ ($alumno->estado_cv == "visible") ? 'selected' : '' }}>visible</option>
            <option {{ ($alumno->estado_cv == "invisible") ? 'selected' : '' }}>invisible</option>
          </select>
      </div>
    </div>

        <div class="form-group row">
        <label id="fam_prof" for="fam_prof" class="col-md-5 col-form-label text-md-right">{{ __('Familia Profesional (CV)') }}</label>

        <div class="col-md-6">
            <input id="fam_prof" type="text" class="form-control @error('fam_prof') is-invalid @enderror" name="fam_prof" value="{{ $alumno->fam_prof }}" autocomplete="fam_prof" autofocus>

            @error('fam_prof')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>

    <div class="form-group row">
      <label for="empleabilidad" class="col-md-5 col-form-label text-md-right">Empleabilidad</label>
      <div class="col-md-6">
          <select class="form-control" id="empleabilidad" name="empleabilidad">
            <option {{ ($alumno->empleabilidad == "disponible") ? 'selected' : '' }}>disponible</option>
            <option {{ ($alumno->empleabilidad == "no disponible") ? 'selected' : '' }}>no disponible</option>
          </select>
      </div>
    </div>
    <div class="form-group row">
      <label for="sit_laboral" class="col-md-5 col-form-label text-md-right">Situación laboral</label>
      <div class="col-md-6">
          <select class="form-control" id="sit_laboral" name="sit_laboral">
            <option {{ ($alumno->sit_laboral == "empleado") ? 'selected' : '' }}>empleado</option>
            <option {{ ($alumno->sit_laboral == "desempleado") ? 'selected' : '' }}>desempleado</option>
          </select>
      </div>
    </div>
    <div class="form-check col-md-4 offset-md-5">
        <input type="checkbox" class="form-check-input" name="check" id="check">
        <label class="form-check-label" for="check" required>Declaro que todos los datos consignados son ciertos</label>
    </div>
    <div class="form-group row mt-2">
        <div class="col-md-7 offset-md-4">
            <button type="submit" class="btn btn-primary btn-block">
                {{ __('Actualizar') }}
            </button>
        </div>
    </div>
</form>