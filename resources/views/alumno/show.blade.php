@extends('layouts.app')

@section('content')
<h2>Alumno</h2>
<div class="list-group">
  <a href="#" class="list-group-item list-group-item-action disabled">Nombre: {{ $alumno->user['name'] }}</a>

  <a href="#" class="list-group-item list-group-item-action disabled">Email: {{ $alumno->user['email'] }}</a>

  <a href="#" class="list-group-item list-group-item-action disabled">Edad: {{ \Carbon\Carbon::parse($alumno->fec_nac)->age }}</a>

  <a href="#" class="list-group-item list-group-item-action disabled">Teléfono: {{ $alumno->tlf ? $alumno->tlf : 'No disponible'}}</a>

  <a href="#" class="list-group-item list-group-item-action disabled">Código Postal: {{ $alumno->cp ? $alumno->cp : 'No disponible' }}</a>

  <a href="#" class="list-group-item list-group-item-action disabled">Familia Profesional: {{ $alumno->fam_prof ? $alumno->fam_prof : 'No disponible' }}</a>

  <a href="#" class="list-group-item list-group-item-action disabled">Titulación: {{ $alumno->titulacion ? $alumno->titulacion : 'No disponible' }}</a>

  <a href="{{ !$alumno->estado_cv || $alumno->estado_cv == 'invisible' || !$alumno->cv ? '#' : url('download', [$alumno->id, explode('/', $alumno->cv)[1]]) }}" class="list-group-item list-group-item-action disabled">CV:
    {{ !$alumno->estado_cv || $alumno->estado_cv == 'invisible' || !$alumno->cv ? 'No disponible' : "" }}
  <span class="text-info">
    {{ $alumno->estado_cv && $alumno->estado_cv != 'invisible' && $alumno->cv ? 'DESCARGAR' : "" }}
  </span></a>

  <a href="#" class="list-group-item list-group-item-action disabled">Estado CV: {{ $alumno->estado_cv }}</a>

  <a href="#" class="list-group-item list-group-item-action disabled">Situación Laboral: {{ $alumno->sit_laboral }}</a>
</div>

  @if ($alumno->id == Auth::user()->alumno['id'])
    <a href="{{ route('alumno.edit', $alumno->id) }}" class="btn btn-success btn-block">Editar</a>
  @endif

<a class="btn btn-secondary btn-block mt-4" href="{{ URL::previous() }}">Volver</a>
@endsection