<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    @if (in_array('index', explode('.', Route::currentRouteName())))
        <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    @endif
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    @if (in_array('index', explode('.', Route::currentRouteName())))
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css">
    @endif

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        @include('includes.header')

<div class="container">
        @if (session('info'))
            <div class="alert alert-info alert-dismissible" id="alert" role="alert" style="position:relative; top: 1em; width: auto;">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                {{ session('info') }}
            </div>
        @endif

        <main class="py-4">
            @yield('content')
            @yield('scripts')
        </main>
    </div>
        @include('includes.footer')
    </div>
</body>
</html>