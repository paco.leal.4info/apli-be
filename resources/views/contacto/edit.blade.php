@extends('layouts.app')

@section('content')
{{ Form::model($contacto, ['route' => ['contacto.update', $contacto], 'method' => 'PUT', 'files' => false]) }}
  @include('contacto.partials.form')
<div>
  {{ Form::submit('Actualizar', ['class' => 'btn btn-primary btn-block']) }}
</div>
{{ Form::close() }}
@endsection