@if (Auth::user()->codigo)
	<div class="form-group">
	  {{ Form::label('empresa_id', 'Empresa') }}
	  {{ Form::select('empresa_id', $arrayEmpresas, null, ['class' => 'form-control', 'id' => 'empresa_id']) }}
	</div>
@endif
<div class="form-group">
  {{ Form::label('nombre', 'Nombre') }}
  {{ Form::text('nombre',null, ['class' => 'form-control', 'id' => 'nombre']) }}
</div>

<div class="form-group">
  {{ Form::label('email', 'Email') }}
  {{ Form::email('email',null, ['class' => 'form-control', 'id' => 'email']) }}
</div>

<div class="form-group">
  {{ Form::label('tlf', 'Teléfono') }}
  {{ Form::text('tlf',null, ['class' => 'form-control', 'id' => 'tlf', 'pattern' => '^[69][0-9]{8}', 'placeholder' => '9######## o 6########']) }}
</div>
