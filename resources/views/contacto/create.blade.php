@extends('layouts.app')

@section('content')
<form method="POST" action="{{ route('contacto.store') }}">
  @csrf
  @include('contacto.partials.form')

  <div class="box-footer">
	  {{ Form::submit('Crear', ['class' => 'btn btn-primary btn-block']) }}
	</div>
</form>
@endsection