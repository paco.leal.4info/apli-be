@extends('layouts.app')

@section('content')
<h2>Contacto</h2>
<div class="list-group">
  <a href="{{ route('empresa.show', $contacto->empresa->id) }}" class="list-group-item list-group-item-action text-primary">Empresa: {{ $contacto->empresa->user['name'] }}</a>
  <a href="#" class="list-group-item list-group-item-action disabled">Nombre: {{ $contacto->nombre }}</a>
  <a href="#" class="list-group-item list-group-item-action disabled">Teléfono: {{ $contacto->tlf }}</a>
  <a href="mailto:{{ $contacto->email }}" class="list-group-item list-group-item-action disabled">Email: {{ $contacto->email }}</a>
  @if ((Auth::user()->empresa['id_fiscal'] && Auth::user()->empresa['id'] == $contacto->empresa->id))
	<a href="{{ route('contacto.edit', $contacto->id) }}" class="btn btn-success btn-block">Editar</a>
  @endif
</div>
<a class="btn btn-secondary btn-block mt-4" href="{{ URL::previous() }}">Volver</a>
@endsection