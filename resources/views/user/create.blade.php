@extends('layouts.app')

@section('content')
<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">
                <span>{{ __('Regístrate') }}</span>
            </div>
            <div class="card-body">
                <form method="POST" action="{{ route('user.store') }}">
                	@include('auth.form')

                    <div class="form-group row mb-0" style="margin-top: 2em;">
                        <div class="col-md-6 offset-md-5">
                            <button type="submit" class="btn btn-primary">
                                {{ __('Crear') }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
function toggle() {
    var valor = document.getElementById('formselect').value;
    var formalumno = document.getElementById('formalumno');
    var formempresa = document.getElementById('formempresa');
    var formprofesor = document.getElementById('formprofesor');
    if (valor == 0) {
        formalumno.style.display = "block";
        formempresa.style.display = "none";
        formprofesor.style.display = "none";
    } else if (valor == 1) {
        formalumno.style.display = "none";
        formempresa.style.display = "block";
        formprofesor.style.display = "none";
    } else if (valor == 2) {
    	formalumno.style.display = "none";
        formempresa.style.display = "none";
        formprofesor.style.display = "block";
    } else {
    	formalumno.style.display = "block";
        formempresa.style.display = "none";
        formprofesor.style.display = "none";
    }
};
</script>
@endsection
