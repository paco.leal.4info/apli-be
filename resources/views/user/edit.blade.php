@extends('layouts.app')

@section('content')
{{ Form::model($user, ['route' => ['user.update', $user], 'method' => 'PUT', 'files' => false]) }}
  <div class="form-group row">
    <label id="name" for="name" class="col-md-5 col-form-label text-md-right">{{ __('Nombre') }}</label>

    <div class="col-md-6">
        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $user->name }}" required autocomplete="name" autofocus>

        @error('name')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
</div>

<div class="form-group row mb-0">
    <div class="col-md-6 offset-md-5">
        <button type="submit" class="btn btn-primary btn-block">
            {{ __('Actualizar') }}
        </button>
    </div>
</div>
{{ Form::close() }}
@endsection