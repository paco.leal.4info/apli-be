@extends('layouts.app')

@section('content')
@php
  $rol = $user->codigo ? 'profesor' : ($user->empresa['id_fiscal'] ? 'empresa' : ($user->alumno['fec_nac'] ? 'alumno' : ''));
@endphp
<h2>Usuario</h2>
<div class="list-group">
  <a href="#" class="list-group-item list-group-item-action disabled">Nombre: {{ $user->name }}</a>
  <a href="#" class="list-group-item list-group-item-action disabled">Rol: {{ $rol }}</a>

  @if ($user->id == Auth::user()->id)
  	<a href="{{ route(($rol != 'profesor' ? $rol : 'user').'.edit', ($rol == 'alumno'? $user->alumno['id'] : ($rol == 'empresa'? $user->empresa['id'] : $user))) }}" class="btn btn-success btn-block">Editar</a>
  @endif
</div>
<a class="btn btn-secondary btn-block mt-4" href="{{ URL::previous() }}">Volver</a>
@endsection