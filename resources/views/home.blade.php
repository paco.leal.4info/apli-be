@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Bienvenido, {{ Auth::user()->name }}</div>

                <div class="card-body">
                    Hola, {{ Auth::user()->name }}, ¿qué te gustaría hacer hoy?
                    @if (Auth::user()->alumno['id'])
                        @if ((!Auth::user()->alumno['cv'] || empty(Auth::user()->alumno['cv'])))
                            Te recomendamos <a href="{{ route('alumno.edit', Auth::user()->alumno['id']) }}">completar la información de tu usuario</a> para comenzar.
                        @endif
                    @endif
                    @if (Auth::user()->empresa['id'])
                        @if (!Auth::user()->empresa['n_empleados'])
                            Te recomendamos <a href="{{ route('empresa.edit', Auth::user()->empresa['id']) }}">completar la información de tu usuario</a> para comenzar.
                        @endif
                        @if ((Auth::user()->empresa->centros->count() < 1) || (Auth::user()->empresa->contactos->count() < 1))
                            Antes de publicar tu primera oferta debes añadir a tu perfil al menos <a href="{{ route('centro.create') }}">un centro de trabajo</a> y <a href="{{ route('contacto.create') }}">un contacto para control de perfiles</a>.
                        @endif
                        @if ((Auth::user()->empresa->ofertas->count() < 1) && (Auth::user()->empresa->centros->count() > 0) && (Auth::user()->empresa->contactos->count() > 0))
                            ¡Ya puedes crear <a href="{{ route('oferta.create') }}">una nueva oferta</a>!
                        @endif
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection