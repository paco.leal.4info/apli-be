@extends('layouts.app')

@section('content')
<form method="POST" action="{{ route('oferta.store') }}">
  @csrf
  @include('oferta.partials.form')

  <div class="box-footer">
	  {{ Form::submit('Crear', ['class' => 'btn btn-primary btn-block']) }}
	</div>
</form>
@endsection