@extends('layouts.app')

@section('content')
{{ Form::model($oferta, ['route' => ['oferta.update', $oferta], 'method' => 'PUT', 'files' => false]) }}
  @include('oferta.partials.form')
<div>
  {{ Form::submit('Actualizar', ['class' => 'btn btn-primary btn-block']) }}
</div>
{{ Form::close() }}
@endsection