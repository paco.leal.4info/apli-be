<div class="form-group">
  {{ Form::label('centro_id', 'Centro') }}
  {{ Form::select('centro_id', $arrayCentros, null, ['class' => 'form-control', 'id' => 'centro_id']) }}
</div>

<div class="form-group">
  {{ Form::label('contacto_id', 'Contacto') }}
  {{ Form::select('contacto_id', $arrayContactos,null, ['class' => 'form-control', 'id' => 'contacto_id']) }}
</div>

<div class="form-group">
  {{ Form::label('vacantes', 'Puestos vacantes') }}
  {{ Form::number('vacantes',null, ['class' => 'form-control', 'id' => 'vacantes', 'min' => 1]) }}
</div>

<div class="form-group">
  {{ Form::label('descripcion', 'Descripción del puesto') }}
  {{ Form::text('descripcion',null, ['class' => 'form-control', 'id' => 'descripcion']) }}
</div>

<div class="form-group">
  {{ Form::label('familia', 'Familia profesional') }}
  {{ Form::text('familia',null, ['class' => 'form-control', 'id' => 'familia']) }}
</div>

<div class="form-group">
  {{ Form::label('perfil', 'Perfil profesional') }}
  {{ Form::select('perfil',$perfil,null, ['class' => 'form-control', 'id' => 'perfil']) }}
</div>

<div class="form-group">
  {{ Form::label('tareas', 'Tareas') }}
  {{ Form::textarea('tareas', null, ['class' => 'form-control', 'id' => 'tareas', 'rows' => '5']) }}
</div>

<div class="form-group">
  {{ Form::label('estado', 'Estado') }}
  {{ Form::select('estado', ['inactiva' => 'inactiva', 'activa' => 'activa'] ,null, ['class' => 'form-control', 'id' => 'estado']) }}
</div>
