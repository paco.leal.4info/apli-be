@extends('layouts.app')

@section('content')
<table class="table table-bordered table-hover" id="example1">
	<thead>
		<tr>
			<th>Empresa</th>
			<th>Nº de vacantes</th>
			<th>Perfil Profesional</th>
			@if (Auth::user()->empresa['id']  || Auth::user()->codigo)
				<th>Estado</th>
			@endif
			<th class="text-center" data-orderable="false">Ver</th>
			@if (Auth::user()->empresa['id'])
				<th class="text-center" data-orderable="false">Editar</th>
			@endif
			@if (Auth::user()->empresa['id'] || Auth::user()->codigo)
				<th class="text-center" data-orderable="false">Borrar</th>
			@endif
		</tr>
	</thead>
	<tbody>
	@foreach ($ofertas as $oferta)
		<tr>
			<td>{{ $oferta->empresa->user['name'] }}</td>
			<td>{{ $oferta->vacantes }}</td>
			<td>{{ $oferta->perfil }}</td>
			@if (Auth::user()->empresa['id'] || Auth::user()->codigo)
				<td>{{ $oferta->estado }}</td>
			@endif
			<td class="text-center">
				<a href="{{ route('oferta.show', $oferta->id) }}" class="btn btn-info">
					<svg class="bi bi-eye-fill text-white" width="1.2em" height="1.2em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
					    <path d="M10.5 8a2.5 2.5 0 1 1-5 0 2.5 2.5 0 0 1 5 0z"/>
					    <path fill-rule="evenodd" d="M0 8s3-5.5 8-5.5S16 8 16 8s-3 5.5-8 5.5S0 8 0 8zm8 3.5a3.5 3.5 0 1 0 0-7 3.5 3.5 0 0 0 0 7z"/>
					</svg>
				</a>
			</td>
				@if (Auth::user()->empresa['id'])
				<td class="text-center">
				<a href="{{ route('oferta.edit', $oferta) }}" class="btn btn-success">
					<svg class="bi bi-pencil text-white" width="1.2em" height="1.2em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
					  	<path fill-rule="evenodd" d="M11.293 1.293a1 1 0 0 1 1.414 0l2 2a1 1 0 0 1 0 1.414l-9 9a1 1 0 0 1-.39.242l-3 1a1 1 0 0 1-1.266-1.265l1-3a1 1 0 0 1 .242-.391l9-9zM12 2l2 2-9 9-3 1 1-3 9-9z"/>
					  	<path fill-rule="evenodd" d="M12.146 6.354l-2.5-2.5.708-.708 2.5 2.5-.707.708zM3 10v.5a.5.5 0 0 0 .5.5H4v.5a.5.5 0 0 0 .5.5H5v.5a.5.5 0 0 0 .5.5H6v-1.5a.5.5 0 0 0-.5-.5H5v-.5a.5.5 0 0 0-.5-.5H3z"/>
					</svg>
				</a>
				</td>
				@endif

				@if (Auth::user()->empresa['id'] || Auth::user()->codigo)
				<td class="text-center">
				{!! Form::open(['route' => ['oferta.destroy', $oferta], 'method' => 'DELETE']) !!}
				<button class="btn btn-danger" onclick="return confirm('¿Estás seguro?')">
					<svg class="bi bi-trash-fill" width="1.2em" height="1.2em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
					  <path fill-rule="evenodd" d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5a.5.5 0 0 0-1 0v7a.5.5 0 0 0 1 0v-7z"/>
					</svg>
				</button>
				{!! Form::close() !!}
				</td>
				@endif

		</tr>
	@endforeach
	</tbody>
</table>
@if (Auth::user()->empresa['id_fiscal'])
	<a href="{{ route('oferta.create') }}">
	<svg class="bi bi-plus-circle-fill text-secondary" width="3em" height="3em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg" style="display:block;margin:auto;">
	<path fill-rule="evenodd" d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM8.5 4a.5.5 0 0 0-1 0v3.5H4a.5.5 0 0 0 0 1h3.5V12a.5.5 0 0 0 1 0V8.5H12a.5.5 0 0 0 0-1H8.5V4z"/>
	</svg>
	</a>
@endif
@endsection
@section('scripts')
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
<script>
  jQuery(function($) {
    $('#example1').DataTable({
    	"order": [[ 1, "desc" ]],
    	"columnDefs": [
                    {
                    	@if (Auth::user()->empresa['id'])
                        	"targets": [ 4, 5, 6 ],
                        @elseif (Auth::user()->alumno['id'])
                        	"targets": [ 3 ],
                		@elseif (Auth::user()->codigo)
                			"targets": [ 4, 5 ],
                        @endif
                        "visible": true,
                        "searchable": false
                    },
        ],
    	"language": {
    		"zeroRecords": "Nada que mostrar",
            "info": "Mostrando _START_ a _END_ de _TOTAL_ ofertas",
            "infoEmpty": "No hay datos disponibles",
            "infoFiltered": "(filtrados de _MAX_ ofertas en total)",
            "sLengthMenu": "Mostrar _MENU_ ofertas",
            "paginate": {
      			"previous": "Anterior",
      			"next": "Siguiente"
    		}
        }
    });
  });
</script>
@endsection