@extends('layouts.app')

@section('content')
<h2>Oferta</h2>
<div class="list-group">
  <a href="{{ route('empresa.show', $oferta->empresa->id) }}" class="list-group-item list-group-item-action text-primary">Nombre Comercial: {{ $oferta->empresa->user['name'] }}</a>
  <a href="{{ route('centro.show', $oferta->centro->id) }}" class="list-group-item list-group-item-action text-primary">Centro de trabajo: {{ $oferta->centro['nombre'] }}</a>
  <a href="{{ route('contacto.show', $oferta->contacto->id) }}" class="list-group-item list-group-item-action text-primary">Contacto: {{ $oferta->contacto['nombre'] }}</a>
  <a href="#" class="list-group-item list-group-item-action disabled">Nº Vacantes: {{ $oferta->vacantes }}</a>
  <a href="#" class="list-group-item list-group-item-action disabled">Familia Profesional: {{ $oferta->familia }}</a>
  <a href="#" class="list-group-item list-group-item-action disabled">Perfil Profesional: {{ $oferta->perfil }}</a>
  <a href="#" class="list-group-item list-group-item-action disabled">Tareas recomendadas: {{ $oferta->tareas }}</a>
  <a href="#" class="list-group-item list-group-item-action disabled">Estado: {{ $oferta->estado }}</a>

  @if (Auth::user()->alumno['id'])
    @php
      $flag = true;
    @endphp
    @foreach (Auth::user()->alumno->inscripciones->all() as $inscripcion)
      @if ($inscripcion['oferta_id'] == $oferta->id)
        @php
           $inscripcionId = $inscripcion->id;
           $flag = false;
        @endphp
        {!! Form::open(['route' => ['inscripcion.destroy', $inscripcionId], 'method' => 'DELETE']) !!}
          <button class="btn btn-danger btn-block" onclick="return confirm('¿Estás seguro?')" style="display:inline-block;">Cancelar inscripción</button>
        {!! Form::close() !!}
      @endif
    @endforeach
    @if ($flag)
    {!! Form::open(['route' => ['inscripcion.store', $oferta->id], 'method' => 'POST']) !!}
          {{ Form::text('ofertaId', $oferta->id, ['hidden']) }}
          <button class="btn btn-primary btn-block" style="display:inline-block;">Inscribirse</button>
        {!! Form::close() !!}
    @endif
  @endif
</div>
<a class="btn btn-secondary btn-block mt-4" href="{{ URL::previous() }}">Volver</a>
@endsection

<!-- TODO: arreglar los botones de cancelar inscripción/inscribirse -->