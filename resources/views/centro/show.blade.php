@extends('layouts.app')

@section('content')
<h2>Centro</h2>
<div class="list-group">
  <a href="{{ route('empresa.show', $centro->empresa->id) }}" class="list-group-item list-group-item-action text-primary">Empresa: {{ $centro->empresa->user['name'] }}</a>
  <a href="#" class="list-group-item list-group-item-action disabled">Nombre: {{ $centro->nombre ? $centro->nombre : 'no especificado' }}</a>
  <a href="#" class="list-group-item list-group-item-action disabled">Dirección: {{ $centro->direccion }}</a>
  <a href="#" class="list-group-item list-group-item-action disabled">Código Postal: {{ $centro->cp }}</a>
  @if ((Auth::user()->empresa['id_fiscal'] && Auth::user()->empresa['id'] == $centro->empresa->id))
  <a href="{{ route('centro.edit', $centro->id) }}" class="btn btn-success btn-block">Editar</a>
  @endif
</div>
<a class="btn btn-secondary btn-block mt-4" href="{{ URL::previous() }}">Volver</a>
@endsection