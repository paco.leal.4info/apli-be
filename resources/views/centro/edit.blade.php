@extends('layouts.app')

@section('content')
{{ Form::model($centro, ['route' => ['centro.update', $centro], 'method' => 'PUT', 'files' => false]) }}
  @include('centro.partials.form')
<div>
  {{ Form::submit('Actualizar', ['class' => 'btn btn-primary btn-block']) }}
</div>
{{ Form::close() }}
@endsection