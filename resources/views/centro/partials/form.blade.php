@if (Auth::user()->codigo)
	<div class="form-group">
	  {{ Form::label('empresa_id', 'Empresa') }}
	  {{ Form::select('empresa_id', $arrayEmpresas, null, ['class' => 'form-control', 'id' => 'empresa_id']) }}
	</div>
@endif

<div class="form-group">
  {{ Form::label('nombre', 'Nombre del centro') }}
  {{ Form::text('nombre',null, ['class' => 'form-control', 'id' => 'nombre']) }}
</div>

<div class="form-group">
  {{ Form::label('direccion', 'Direccion del centro') }}
  {{ Form::text('direccion',null, ['class' => 'form-control', 'id' => 'direccion']) }}
</div>

<div class="form-group">
  {{ Form::label('cp', 'Código Postal') }}
  {{ Form::text('cp',null, ['class' => 'form-control', 'id' => 'cp', 'pattern' => '[0-9]{5}', 'placeholder' => '#####']) }}
</div>
