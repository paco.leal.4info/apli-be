<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Oferta extends Model
{
    protected $table = 'ofertas';
    protected $fillable = [
        'empresa_id',
        'centro_id',
        'contacto_id',
        'vacantes',
        'descripcion',
        'familia',
        'perfil',
        'tareas',
        'estado'
    ];

    public function empresa()
    {
        return $this->belongsTo('App\Empresa');
    }

    public function centro()
    {
        return $this->belongsTo('App\Centro');
    }

    public function contacto()
    {
        return $this->belongsTo('App\Contacto');
    }

    public function inscripciones()
    {
        return $this->hasMany('App\Inscripcion', 'alumno_id', 'id');
    }
}
