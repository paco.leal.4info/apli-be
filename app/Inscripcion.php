<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inscripcion extends Model
{
    protected $table = 'inscripciones';
    protected $fillable = [
        'alumno_id',
        'oferta_id',
        'estado',
    ];

    public function alumno()
    {
        return $this->belongsTo('App\Alumno');
    }

    public function oferta()
    {
        return $this->belongsTo('App\Oferta');
    }
}
