<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contacto extends Model
{
    protected $table = 'contactos';
    protected $fillable = [
        'empresa_id',
        'nombre',
        'tlf',
        'email',
    ];

    public function empresa()
    {
        return $this->belongsTo('App\Empresa');
    }

    public function ofertas()
    {
        return $this->hasMany('App\Oferta');
    }
}
