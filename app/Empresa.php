<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{
    protected $table = 'empresas';

    protected $fillable = [
        'user_id',
        'nombre_s',
        'nombre_c',
        'id_fiscal',
        'dir_social',
        'actividad',
        'cnae_iae',
        'web',
        'tlf',
        'n_empleados',
        'alta_prof'
    ];

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    public function centros()
    {
        return $this->hasMany('App\Centro');
    }

    public function contactos()
    {
        return $this->hasMany('App\Contacto');
    }

    public function ofertas()
    {
        return $this->hasMany('App\Oferta');
    }
}
