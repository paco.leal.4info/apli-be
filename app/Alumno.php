<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Alumno extends Model
{
    protected $table = 'alumnos';

    // campos rellenables
    protected $fillable = [
        'user_id',
        'nombre',
        'fec_nac',
        'tlf',
        'cp',
        'fam_prof',
        'titulacion',
        'cv',
        'estado_cv',
        'empleabilidad',
        'sit_laboral'
    ];

    // relaciones
    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    public function inscripciones()
    {
        return $this->hasMany('App\Inscripcion', 'alumno_id', 'id');
    }
}
