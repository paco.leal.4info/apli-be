<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Alumno;
use App\Empresa;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';
    // TODO: Redirigir a /user/id del usuario recién creado

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        // dd($data);
        if ($data['formselect'] != "1" && $data['formselect'] != "0") {
            // error (a controlar en validación)
            session(['status' => "No se ha recibido el formulario correctamente"]);
            return redirect('register');
        }

        $usercreated = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);

        if ($data['formselect'] == "1") {
            //codigo para añadir empresa
            Empresa::create([
                'user_id' => $usercreated->id,
                'nombre_s' => $data['nombre_s'],
                'id_fiscal' => $data['id_fiscal'],
                'dir_social' => $data['dir_social'],
                'actividad' => $data['actividad'],
                'cnae_iae' => $data['cnae_iae'],
            ]);
        } else if ($data['formselect'] == "0") {
            //codigo para añadir alumno
            Alumno::create([
                'user_id' => $usercreated->id,
                'fec_nac' => $data['fec_nac'],
                'tlf' => $data['tlf'],
                'cp' => $data['cp'],
            ]);
        }

        return $usercreated;
    }
}
