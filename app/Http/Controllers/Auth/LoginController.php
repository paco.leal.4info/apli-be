<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
//use App\Traits\RoleManagementTrait;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */
    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
       //session(['status' => "Puede empezar por añadir <a href='".route(($RoleManagemenTrait::checkUserRole() == 'profesor' ? '' : $this.checkUserRole()).'.edit')."'>el resto de sus datos</a>"]);
    }

/*
    public function RoleManagementTrait() {
        dispatch(new RoleManagementTrait());
    }
*/
}
