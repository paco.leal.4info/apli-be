<?php

namespace App\Http\Controllers;

use App\Functions;
use App\Alumno;
use Illuminate\Http\Request;

class DownloadController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function download($id, $pdf) {
    	$alumno = Alumno::find($id);
    	if (Functions::setValidRole(['alumno']) && !Functions::setValidId($alumno->id)) {
            return abort(404);
    	}
    	return response()->download(public_path($alumno->cv));
    }
}
