<?php

namespace App\Http\Controllers;

use App\Alumno;
use App\User;
use App\Functions;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class AlumnoController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Functions::setValidRole(['alumno'])) {
            return abort(404);
        }
        $alumnos = Alumno::all();
        if (!$alumnos) { return abort(404); }
        return view('alumno.index', compact('alumnos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return abort(404);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Alumno  $alumno
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $alumno = Alumno::find($id);
        if (!$alumno) { return abort(404); }
        if (Functions::setValidRole(['alumno']) && !Functions::setValidId($alumno->id)) {
            return abort(404);
        }
        return view('alumno.show', compact('alumno'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Alumno  $alumno
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $alumno = Alumno::find($id);
        $titulaciones = Functions::getOpciones();
        if (!$alumno) { return abort(404); }
        if (Functions::setValidRole(['alumno', 'empresa']) && !Functions::setValidId($alumno->id)) {
            return abort(404);
        }
        return view('alumno.edit', compact('alumno', 'titulaciones'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Alumno  $alumno
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $alumno = Alumno::find($id);
        if (Functions::setValidRole(['empresa', 'alumno']) && !Functions::setValidId($alumno->id)) {
            return abort(404);
        }
        $data = $request->all();
        if (!$alumno || !$data) { return abort(404); }
        $alumno->fill($data)->save();
        User::find($alumno->user_id)->fill($data)->save();
        if ($request->file('cv')) {
            $path = Storage::disk('public')->put('CVs', $request->file('cv'));
            $alumno->fill(['cv' => $path])->save();
        }

        if (Functions::setValidRole(['profesor'])) {
            return redirect()->route('user.index')->with('info', 'Alumno actualizado');
        }

        return redirect()->route((Functions::setValidRole(['alumno'])? 'home' : 'alumno.index'))->with('info', 'Alumno actualizado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Alumno  $alumno
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Functions::setValidRole(['empresa', 'alumno'])) {
            return abort(404);
        }
        $alumno = Alumno::find($id);
        if (!$alumno) { return abort(404); }
        User::find($alumno->user_id)->delete();
        return redirect()->route('alumno.index')->with('info', 'Alumno eliminado');
    }
}
