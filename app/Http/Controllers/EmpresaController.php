<?php

namespace App\Http\Controllers;

use App\Empresa;
use App\User;
use App\Functions;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EmpresaController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Functions::setValidRole(['empresa'])) {
            return abort(404);
        }
        $empresas = Empresa::all();
        if (!$empresas) { return abort(404); }
        return view('empresa.index', compact('empresas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return abort(404);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Empresa  $empresa
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $empresa = Empresa::find($id);
        if (!$empresa) { return abort(404); }
        if (Functions::setValidRole(['empresa']) && !Functions::setValidId($empresa->id)) {
            return abort(404);
        }
        return view('empresa.show', compact('empresa'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Empresa  $empresa
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $empresa = Empresa::find($id);
        //dd($empresa);
        if (!$empresa) { return abort(404); }
        if (Functions::setValidRole(['empresa', 'alumno']) && !Functions::setValidId($empresa->id)) {
            return abort(404);
        }
        return view('empresa.edit', compact('empresa'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Empresa  $empresa
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $empresa = Empresa::find($id);
        if (Functions::setValidRole(['empresa', 'alumno']) && !Functions::setValidId($empresa->id)) {
            return abort(404);
        }
        $data = $request->all();
        if (!$empresa || !$data) { return abort(404); }
        $empresa->fill($data)->save();
        User::find($empresa->user_id)->fill($data)->save();

        if (Functions::setValidRole(['profesor'])) {
            return redirect()->route('user.index')->with('info', 'Empresa actualizada');
        }

        return redirect()->route((Functions::setValidRole(['empresa'])? 'home' : 'empresa.index'))->with('info', 'Empresa actualizada');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Empresa  $empresa
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!Functions::setValidRole(['profesor'])) {
            return abort(404);
        }
        $empresa = Empresa::find($id);
        if (!$empresa) { return abort(404); }
        User::find($empresa->user_id)->delete();
        return redirect()->route('empresa.index')->with('info', 'Empresa eliminada');
    }
}
