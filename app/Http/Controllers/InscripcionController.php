<?php

namespace App\Http\Controllers;

use App\Inscripcion;
use App\Oferta;
use App\Functions;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class InscripcionController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /*
        if (Functions::setValidRole(['empresa', 'alumno']) && !Functions::setValidId($oferta->empresa->user_id)) {
            return abort(404);
        }*/
        if (Functions::setValidRole(['profesor'])) {
            $inscripciones = Inscripcion::all();
        } else if (Functions::setValidRole(['alumno'])) {
            $inscripciones = Inscripcion::all()->where('alumno_id', Auth::user()->alumno['id']);
        } else if (Functions::setValidRole(['empresa'])) {
            $ofertas = Auth::user()->empresa->ofertas;
            $arrayOfertasId = null;
            foreach ($ofertas as $oferta) {
                $arrayOfertasId[] = $oferta->id;
            }
            $inscripciones = Inscripcion::all()->whereIn('oferta_id', $arrayOfertasId);
        }
        if (!$inscripciones) { return abort(404); }

        return view('inscripcion.index', compact('inscripciones'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // EN DESUSO
        return abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        Inscripcion::create([
            'alumno_id' => Auth::user()->alumno['id'],
            'oferta_id' => $data['ofertaId'],
        ]);
        return redirect()->route('oferta.index')->with('info', 'Inscripción completada');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // EN DESUSO
        return abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $inscripcion = Inscripcion::find($id);
        if (Functions::setValidRole(['empresa', 'alumno']) && !Functions::setValidId($inscripcion->oferta->empresa_id)) {
            return abort(404);
        }
        $estado = Functions::getEstadOferta();
        return view('inscripcion.edit', compact('inscripcion', 'estado'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $inscripcion = Inscripcion::find($id);
        if (Functions::setValidRole(['empresa', 'alumno']) && !Functions::setValidId($inscripcion->oferta->empresa_id)) {
            return abort(404);
        }
        $data = $request->all();
        $inscripcion->fill($data)->save();
        return redirect()->route('inscripcion.index')->with('info', 'Inscripción actualizada');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $inscripcion = Inscripcion::find($id);
        if (!$inscripcion) { return abort(404); }
        if (Functions::setValidRole(['empresa'])) {
            return abort(404);
        } else if (Functions::setValidRole(['alumno', 'empresa']) && !Functions::setValidId($inscripcion->alumno_id)) {
            return abort(404);
        }
        $inscripcion->delete();
        return redirect()->route('oferta.index')->with('info', 'Inscripción eliminada');
    }
}
