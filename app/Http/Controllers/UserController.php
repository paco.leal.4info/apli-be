<?php

namespace App\Http\Controllers;

use App\User;
use App\Alumno;
use App\Empresa;
use App\Functions;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
//use App\Traits\RoleManagerTrait;

class UserController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!Functions::setValidRole(['profesor'])) {
            return abort(404);
        }
        $users = User::all();
        if (!$users) { return abort(404); }
        return view('user.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!Functions::setValidRole(['profesor'])) {
            return abort(404);
        }
        return view('user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!Functions::setValidRole(['profesor'])) {
            return abort(404);
        }
        $data = $request->all();
        //dd($data);
        if (!$data) { return abort(404); }

        if ($data['codigo'] && !empty($data['codigo'])) {
            $codigo = Hash::make($data['codigo']);
        } else {
            $codigo = null;
        }

        $usercreated = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'codigo' => $codigo,
        ]);

        if ($data['formselect'] == "1") {
            //codigo para añadir empresa
            Empresa::create([
                'user_id' => $usercreated->id,
                'nombre_s' => $data['nombre_s'],
                'id_fiscal' => $data['id_fiscal'],
                'dir_social' => $data['dir_social'],
                'actividad' => $data['actividad'],
                'cnae_iae' => $data['cnae_iae'],
                'alta_prof' => 1,
            ]);
        } else if ($data['formselect'] == "0") {
            //codigo para añadir alumno
            Alumno::create([
                'user_id' => $usercreated->id,
                'fec_nac' => $data['fec_nac'],
                'tlf' => $data['tlf'],
                'cp' => $data['cp'],
            ]);
        }

        return redirect()->route('user.index')->with('info', 'Usuario creado');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (Functions::setValidRole(['empresa', 'alumno']) && !Functions::setValidId($id)) {
            return abort(404);
        }
        $user = User::find($id);
        if (!$user) { return abort(404); }
        return view('user.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Functions::setValidRole(['empresa', 'alumno']) && !Functions::setValidId($id)) {
            return abort(404);
        }
        $user = User::find($id);
        if (!$user) { return abort(404); }
        if (Auth::user()->id != $id && !Auth::user()->codigo) {
            return abort(404);
        }
        if (Auth::user()->empresa['id']) {
            $empresa = $user->empresa;
            return view('empresa.edit', compact('empresa'));
        } else if (Auth::user()->alumno['id']) {
            $alumno = $user->alumno;
            $titulaciones = Functions::getOpciones();
            return view('alumno.edit', compact('alumno', 'titulaciones'));
        }
        return view('user.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (Functions::setValidRole(['empresa', 'alumno']) && !Functions::setValidId($id)) {
            return abort(404);
        }
        $user = User::find($id);
        $data = $request->all();
        if (!$user || !$data) { return abort(404); }
        $user->fill($data)->save();
        return redirect()->route('user.index')->with('info', 'Usuario actualizado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Functions::setValidRole(['empresa', 'alumno']) || Functions::setValidId($id)) {
            return abort(404);
        }
        $user = User::find($id);
        if (!$user) { return abort(404); }
        $user->delete();
        return redirect()->route('user.index')->with('info', 'Usuario eliminado');
    }
}
