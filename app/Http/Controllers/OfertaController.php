<?php

namespace App\Http\Controllers;

use App\Oferta;
use App\Functions;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class OfertaController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Functions::setValidRole(['empresa'])) {
            $ofertas = Oferta::all()->where('empresa_id', Auth::user()->empresa['id']);
        } else if (Functions::setValidRole(['profesor'])) {
            $ofertas = Oferta::all();
        } else {
            $ofertas = Oferta::all()->where('estado', 'activa');
        }
        if (!$ofertas) { return abort(404); }
        return view('oferta.index', compact('ofertas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!Functions::setValidRole(['empresa'])) {
            return abort(404);
        }

        $centros = Auth::user()->empresa->centros;
        $arrayCentros = null;
        foreach ($centros as $centro) {
            $arrayCentros[$centro->id] = $centro->nombre.' - '.$centro->direccion;
            //$arrayCentros.put($centro->id);
        }

        $contactos = Auth::user()->empresa->contactos;
        $arrayContactos = null;
        foreach ($contactos as $contacto) {
            $contactoPreferido = $contacto->email ? $contacto->email : $contacto->tlf;
            $arrayContactos[$contacto->id] = $contacto->nombre.' - '.$contactoPreferido;
            //$arrayCentros.put($centro->id);
        }

        if (!$arrayCentros || !$arrayContactos || empty($arrayCentros) || empty($arrayContactos)) {
            return redirect()->route('home')->with('info', 'Debes crear antes un centro y un contacto');
        }

        $perfil = Functions::getOpciones();
        //dd($arrayCentros);
        return view('oferta.create', compact('arrayCentros', 'arrayContactos', 'perfil'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!Functions::setValidRole(['empresa'])) {
            return abort(404);
        }
        $data = $request->all();
        $data['empresa_id'] = Auth::user()->empresa['id'];
        $oferta = Oferta::create($data);
        if (!$oferta) { return abort(404); }
        return redirect()->route('oferta.index')->with('info', 'Oferta creada');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Oferta  $oferta
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $oferta = Oferta::find($id);
        if (!$oferta) { return abort(404); }
        if (Functions::setValidRole(['empresa']) && !Functions::setValidId($oferta->empresa->id)) {
            return abort(404);
        }
        return view('oferta.show', compact('oferta'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Oferta  $oferta
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $oferta = Oferta::find($id);

        $centros = Auth::user()->empresa->centros;
        $arrayCentros;
        foreach ($centros as $centro) {
            $arrayCentros[$centro->id] = $centro->nombre.' - '.$centro->direccion;
            //$arrayCentros.put($centro->id);
        }

        $contactos = Auth::user()->empresa->contactos;
        $arrayContactos;
        foreach ($contactos as $contacto) {
            $contactoPreferido = $contacto->email ? $contacto->email : $contacto->tlf;
            $arrayContactos[$contacto->id] = $contacto->nombre.' - '.$contactoPreferido;
            //$arrayCentros.put($centro->id);
        }

        if (!$oferta || !$arrayContactos || !$arrayCentros) { return abort(404); }

        if (Functions::setValidRole(['empresa', 'alumno', 'profesor']) && !Functions::setValidId($oferta->empresa->id)) {
            return abort(404);
        }

        $perfil = Functions::getOpciones();
        return view('oferta.edit', compact('oferta', 'arrayCentros', 'arrayContactos','perfil'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Oferta  $oferta
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $oferta = Oferta::find($id);
        $data = $request->all();
        if (!$oferta || !$data) { return abort(404); }
        if (Functions::setValidRole(['empresa', 'alumno', 'profesor']) && !Functions::setValidId($oferta->empresa->id)) {
            return abort(404);
        }
        $oferta->fill($data)->save();
        return redirect()->route('oferta.index')->with('info', 'Oferta actualizada');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Oferta  $oferta
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $oferta = Oferta::find($id);
        if (!$oferta) { return abort(404); }
        if (Functions::setValidRole(['empresa', 'alumno']) && !Functions::setValidId($oferta->empresa->id)) {
            return abort(404);
        }
        $oferta->delete();
        return redirect()->route('oferta.index')->with('info', 'Oferta eliminada');
    }
}
