<?php

namespace App\Http\Controllers;

use App\Contacto;
use App\Empresa;
use App\Functions;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ContactoController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Functions::setValidRole(['empresa'])) {
            $contactos = Contacto::all()->where('empresa_id', Auth::user()->empresa['id']);
        } else if (Functions::setValidRole(['profesor'])) {
            $contactos = Contacto::all();
        } else{
            return abort(404);
        }
        if (!$contactos) { return abort(404); }
        return view('contacto.index', compact('contactos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Functions::setValidRole(['profesor'])) {
            $empresas = Empresa::all();
            $arrayEmpresas = null;
            foreach ($empresas as $empresa) {
                $arrayEmpresas[$empresa->id] = $empresa->user['name'];
            }
            return view('contacto.create', compact('arrayEmpresas'));
        } else if (Functions::setValidRole(['alumno'])) {
            return abort(404);
        }
        return view('contacto.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Functions::setValidRole(['alumno'])) {
            return abort(404);
        }
        $data = $request->all();

        if (!$data) { return abort(404); }

        Contacto::create([
            'empresa_id' => isset($data['empresa_id']) ? $data['empresa_id'] : Auth::user()->empresa['id'],
            'nombre' => $data['nombre'],
            'tlf' => $data['tlf'],
            'email' => $data['email'],
        ]);

        return redirect()->route('contacto.index')->with('info', 'Contacto creado');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Contacto  $contacto
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $contacto = Contacto::find($id);
        if (!$contacto) { return abort(404); }
        if (Functions::setValidRole(['empresa']) && !Functions::setValidId($contacto->empresa->id)) {
            return abort(404);
        }
        return view('contacto.show', compact('contacto'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Contacto  $contacto
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $contacto = Contacto::find($id);
        if (!$contacto) { return abort(404); }
        if (Functions::setValidRole(['empresa', 'alumno']) && !Functions::setValidId($contacto->empresa->id)) {
            return abort(404);
        }
        if (Functions::setValidRole(['profesor'])) {
            $empresas = Empresa::all();
            $arrayEmpresas = null;
            foreach ($empresas as $empresa) {
                $arrayEmpresas[$empresa->id] = $empresa->user['name'];
            }
            return view('contacto.edit', compact('arrayEmpresas', 'contacto'));
        }
        return view('contacto.edit', compact('contacto'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Contacto  $contacto
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $contacto = Contacto::find($id);
        $data = $request->all();
        if (!$contacto || !$data) { return abort(404); }
        if (Functions::setValidRole(['empresa', 'alumno']) && !Functions::setValidId($contacto->empresa->id)) {
            return abort(404);
        }
        $contacto->fill($data)->save();
        return redirect()->route('contacto.index')->with('info', 'Contacto actualizado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Contacto  $contacto
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $contacto = Contacto::find($id);
        if (!$contacto) { return abort(404); }
        if (Functions::setValidRole(['empresa', 'alumno']) && !Functions::setValidId($contacto->empresa->id)) {
            return abort(404);
        }
        $contacto->delete();
        return redirect()->route('contacto.index')->with('info', 'Contacto eliminado');
    }
}
