<?php

namespace App\Http\Controllers;

use App\Centro;
use App\Empresa;
use App\Functions;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CentroController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Functions::setValidRole(['empresa'])) {
            $centros = Centro::all()->where('empresa_id', Auth::user()->empresa['id']);
        } else if (Functions::setValidRole(['profesor'])) {
            $centros = Centro::all();
        } else {
            return abort(404);
        }
        if (!$centros) { return abort(404); }
        return view('centro.index', compact('centros'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Functions::setValidRole(['alumno'])) {
            return abort(404);
        } else if (Functions::setValidRole(['profesor'])) {
            $empresas = Empresa::all();
            $arrayEmpresas = null;
            foreach ($empresas as $empresa) {
                $arrayEmpresas[$empresa->id] = $empresa->user['name'];
            }
            return view('centro.create', compact('arrayEmpresas'));
        }
        return view('centro.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Functions::setValidRole(['alumno'])) {
            return abort(404);
        }
        $data = $request->all();

        Centro::create([
            'empresa_id' => isset($data['empresa_id']) ? $data['empresa_id'] : Auth::user()->empresa['id'],
            'direccion' => $data['direccion'],
            'cp' => $data['cp'],
            'nombre' => $data['nombre'],
        ]);

        return redirect()->route('centro.index')->with('info', 'Centro creado');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Centro  $centro
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $centro = Centro::find($id);
        if (!$centro) { return abort(404); }
        if (Functions::setValidRole(['empresa']) && !Functions::setValidId($centro->empresa->id)) {
            return abort(404);
        }
        return view('centro.show', compact('centro'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Centro  $centro
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $centro = Centro::find($id);
        if (!$centro) { return abort(404); }
        if (Functions::setValidRole(['empresa', 'alumno']) && !Functions::setValidId($centro->empresa->id)) {
            return abort(404);
        }
        if (Functions::setValidRole(['profesor'])) {
            $empresas = Empresa::all();
            $arrayEmpresas = null;
            foreach ($empresas as $empresa) {
                $arrayEmpresas[$empresa->id] = $empresa->user['name'];
            }
            return view('centro.edit', compact('arrayEmpresas', 'centro'));
        }
        return view('centro.edit', compact('centro'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Centro  $centro
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $centro = Centro::find($id);
        $data = $request->all();
        if (!$centro || !$data) { return abort(404); }
        if (Functions::setValidRole(['empresa', 'alumno']) && !Functions::setValidId($centro->empresa->id)) {
            return abort(404);
        }
        $centro->fill($data)->save();
        return redirect()->route('centro.index')->with('info', 'Centro actualizado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Centro  $centro
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $centro = Centro::find($id);
        if (!$centro) { return abort(404); }
        if (!Functions::setValidRole(['profesor']) && !Functions::setValidId($centro->empresa->id)) {
            return abort(404);
        }
        $centro->delete();
        return redirect()->route('centro.index')->with('info', 'Centro eliminado');
    }
}
