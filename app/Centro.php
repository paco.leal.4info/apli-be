<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Centro extends Model
{
    protected $table = 'centros';

    protected $fillable = [
        'empresa_id',
        'nombre',
        'direccion',
        'cp'
    ];

    public function empresa()
    {
        return $this->belongsTo('App\Empresa');
    }

    public function ofertas()
    {
        return $this->hasMany('App\Oferta');
    }
}
