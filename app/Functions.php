<?php

namespace App;

use Illuminate\Support\Facades\Auth;

class Functions
{
	// Para añadir o quitar opciones de titulación/perfil profesional
	public static $opciones = [
       'Ninguna' => 'Ninguna',
	     'Actividades comerciales, grado medio' => 'Actividades comerciales, grado medio',
       'Gestión administrativa, grado medio' => 'Gestión administrativa, grado medio',
       'Sistemas microinformáticos y redes, grado medio' => 'Sistemas microinformáticos y redes, grado medio',
       'Transporte y logística, grado superior' =>'Transporte y logística, grado superior',
       'Comercio internacional bilingüe, grado superior' => 'Comercio internacional bilingüe, grado superior',
       'Marketing y publicidad, grado superior' => 'Marketing y publicidad, grado superior',
       'Administración y finanzas, grado superior' => 'Administración y finanzas, grado superior',
       'Administración de sistemas informáticos en red, grado superior' => 'Administración de sistemas informáticos en red, grado superior',
       'Desarrollo de Aplicaciones WEB, grado superior' => 'Desarrollo de Aplicaciones WEB, grado superior',
       'Desarrollo de Aplicaciones Multiplataforma, grado superior' => 'Desarrollo de Aplicaciones Multiplataforma, grado superior'
   ];

	public static $estadOferta = [
		'inscrito' => 'Inscrito',
		'seleccion' => 'En proceso de seleccion',
		'finalizado' => 'Finalizado'
	];

	public static function getOpciones() {
		return self::$opciones;
	}

	public static function getEstadOferta() {
		return self::$estadOferta;
	}

	// devuelve tu rol o te redirige a un 404
    public static function checkUserRole() {
        if (Auth::user()->codigo) {
            return 'profesor';
        } else if (Auth::user()->empresa['id_fiscal']) {
            return 'empresa';
        } else if (Auth::user()->alumno['fec_nac']) {
            return 'alumno';
        } else {
            return null;
        }
    }

    // compara tu rol con el proporcionado
    public static function setValidRole($rolVal) {
        if (in_array(self::checkUserRole(), $rolVal)) {
            return true;
        } else {
            return false;
        }
    }

    // Comprueba si tu userid, empresaid o alumnoid coincide con el pasado
    public static function setValidId($methodId) {
        if (Auth::user()->codigo) {
            return (Auth::user()->id == $methodId);
        } else if (Auth::user()->empresa['id']) {
            return (Auth::user()->empresa['id'] == $methodId);
        } else if (Auth::user()->alumno['id']) {
            return (Auth::user()->alumno['id'] == $methodId);
        } else {
            return false;
        }
    }
}