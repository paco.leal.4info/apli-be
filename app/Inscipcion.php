<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inscipcion extends Model
{
    protected $table = 'inscripciones';
    protected $fillable = [
        'alumno_id',
        'oferta_id',
        'estado',
    ];

    public function alumnos()
    {
        return $this->belongsToMany('App\Alumno');
    }
}
