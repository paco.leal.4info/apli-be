<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::view('/', 'welcome', ['name' => 'Prueba'])->name('welcome');

Route::resource('alumno', 'AlumnoController');
Route::resource('user', 'UserController');
Route::resource('empresa', 'EmpresaController');
Route::resource('centro', 'CentroController');
Route::resource('contacto', 'ContactoController');
Route::resource('oferta', 'OfertaController');
Route::resource('inscripcion', 'InscripcionController');

Route::get('download/{id}/{pdf}', 'DownloadController@download');