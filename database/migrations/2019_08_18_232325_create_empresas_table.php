<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpresasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empresas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id')->unique();
            $table->string('nombre_s')->nullable(false);
            $table->string('id_fiscal')->nullable(false);
            $table->string('dir_social')->nullable(false);
            $table->enum('actividad', ['actividad1', 'actividad2', 'actividad3', 'etc'])->default('actividad1');
            $table->string('cnae_iae')->nullable(false);
            $table->string('web')->nullable();
            $table->string('tlf')->nullable();
            $table->integer('n_empleados')->unsigned()->nullable();
            $table->boolean('alta_prof')->nullable();
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('empresas');
    }
}
