<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlumnosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alumnos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id')->unique();
            $table->date('fec_nac')->nullable(false);
            $table->string('tlf')->nullable();
            $table->string('cp')->nullable();
            $table->string('fam_prof')->nullable();
            $table->string('titulacion')->nullable();
            $table->string('cv')->nullable();
            $table->enum('estado_cv', ['visible', 'invisible'])->default('invisible');
            $table->enum('empleabilidad', ['disponible', 'no disponible'])->default('disponible');
            $table->enum('sit_laboral', ['empleado', 'desempleado'])->default('desempleado');
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alumnos');
    }
}
