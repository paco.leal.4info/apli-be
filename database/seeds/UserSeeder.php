<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'alumno',
            'email' => 'alumno@email.com',
            'password' => Hash::make('password'),
        ]);

        DB::table('users')->insert([
            'name' => 'alumno2',
            'email' => 'alumno2@email.com',
            'password' => Hash::make('password'),
        ]);

        DB::table('users')->insert([
            'name' => 'alumno3',
            'email' => 'alumno3@email.com',
            'password' => Hash::make('password'),
        ]);

        DB::table('users')->insert([
            'name' => 'alumno4',
            'email' => 'alumno4@email.com',
            'password' => Hash::make('password'),
        ]);

        DB::table('users')->insert([
            'name' => 'empresa',
            'email' => 'empresa@email.com',
            'password' => Hash::make('password'),
        ]);

        DB::table('users')->insert([
            'name' => 'empresa2',
            'email' => 'empresa2@email.com',
            'password' => Hash::make('password'),
        ]);

        DB::table('users')->insert([
            'name' => 'empresa3',
            'email' => 'empresa3@email.com',
            'password' => Hash::make('password'),
        ]);

        DB::table('users')->insert([
            'name' => 'empresa4',
            'email' => 'empresa4@email.com',
            'password' => Hash::make('password'),
        ]);

        DB::table('users')->insert([
            'name' => 'profesor',
            'email' => 'profesor@email.com',
            'password' => Hash::make('password'),
            'codigo' => Str::random(10),
        ]);

        DB::table('users')->insert([
            'name' => 'profesor2',
            'email' => 'profesor2@email.com',
            'password' => Hash::make('password'),
            'codigo' => Str::random(10),
        ]);
    }
}
