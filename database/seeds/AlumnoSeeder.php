<?php

use Illuminate\Database\Seeder;

class AlumnoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('alumnos')->insert([
        	'user_id' => 1,
            'fec_nac' => '1996-05-13',
            'tlf' => '654654654',
            'cp' => '32121',
            'fam_prof' => 'Tecnología y Comunicaciones',
            'titulacion' => 'Desarrollo de Aplicaciones WEB, grado superior',
            'estado_cv' => 'invisible',
            'empleabilidad' => 'disponible',
            'sit_laboral' => 'desempleado',
        ]);

        DB::table('alumnos')->insert([
            'user_id' => 2,
            'fec_nac' => '1990-03-21',
            'tlf' => '987987987',
            'cp' => '32121',
            'fam_prof' => 'Tecnología y Comunicaciones',
            'titulacion' => 'Desarrollo de Aplicaciones Multiplataforma, grado superior',
            'estado_cv' => 'invisible',
            'empleabilidad' => 'disponible',
            'sit_laboral' => 'desempleado',
        ]);

        DB::table('alumnos')->insert([
            'user_id' => 3,
            'fec_nac' => '1990-03-21',
            'tlf' => '987987987',
            'cp' => '32121',
        ]);

        DB::table('alumnos')->insert([
            'user_id' => 4,
            'fec_nac' => '1990-03-21',
        ]);
    }
}
