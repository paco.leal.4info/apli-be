<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // La creación de datos de usuarios debe ejecutarse primero, ya que alumnos y empresas dependen de ellos
        $this->call(UserSeeder::class);

        $this->call(AlumnoSeeder::class);

        $this->call(EmpresaSeeder::class);

        // Los tres últimos seeders dependen de empresa y además oferta depende también de estos dos primeros, así como inscripción de oferta
        $this->call(CentroSeeder::class);

        $this->call(ContactoSeeder::class);

        $this->call(OfertaSeeder::class);

        $this->call(InscripcionSeeder::class);

    }
}
