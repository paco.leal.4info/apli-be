<?php

use Illuminate\Database\Seeder;

class CentroSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('centros')->insert([
        	'empresa_id' => 1,
        	'direccion' => 'Calle falsa, 12',
            'cp' => '54321',
            'nombre' => 'Oficinas flotantes',
        ]);

        DB::table('centros')->insert([
            'empresa_id' => 1,
            'direccion' => 'Calle verdadera, 21',
            'cp' => '32154',
            'nombre' => 'Oficinas subterráneas',
        ]);

        DB::table('centros')->insert([
            'empresa_id' => 2,
            'direccion' => 'Calle nueva, 23',
            'cp' => '32154',
            'nombre' => 'Oficinas centrales',
        ]);
    }
}
