<?php

use Illuminate\Database\Seeder;

class ContactoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('contactos')->insert([
        	'empresa_id' => 1,
        	'nombre' => 'Fernando Aranjuez Cruz',
            'tlf' => '684684684',
            'email' => 'feracruz@email.com',
        ]);

        DB::table('contactos')->insert([
            'empresa_id' => 1,
            'nombre' => 'Mercedes Palermo López',
            'tlf' => '957957957',
            'email' => 'merpalo@email.com',
        ]);

        DB::table('contactos')->insert([
            'empresa_id' => 2,
            'nombre' => 'Claudia Sánchez Rodríguez',
            'tlf' => '624624624',
            'email' => 'clausanro@email.com',
        ]);
    }
}
