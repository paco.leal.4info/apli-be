<?php

use Illuminate\Database\Seeder;

class EmpresaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('empresas')->insert([
        	'user_id' => 5,
        	'nombre_s' => 'Empresa SL',
            'id_fiscal' => '00000000T',
            'dir_social' => 'Dirección Social de pruebas',
            'actividad' => 'etc',
            'cnae_iae' => 'No se cuenta con un servicio de CNAE',
            'web' => 'http://www.web.com',
            'tlf' => '987987987',
            'n_empleados' => '420',
        ]);

        DB::table('empresas')->insert([
            'user_id' => 6,
            'nombre_s' => 'Empresa2 SA',
            'id_fiscal' => 'Q00000000',
            'dir_social' => 'Dirección Social de pruebas 2',
            'actividad' => 'actividad2',
            'cnae_iae' => 'No se cuenta con un servicio de CNAE',
            'web' => 'http://www.web2.com',
            'tlf' => '654654654',
            'n_empleados' => '69',
        ]);

        DB::table('empresas')->insert([
            'user_id' => 7,
            'nombre_s' => 'Empresa 3',
            'id_fiscal' => 'Q00000000',
            'dir_social' => 'Dirección Social de pruebas 3',
            'actividad' => 'actividad3',
            'cnae_iae' => 'No se cuenta con un servicio de CNAE',
            'n_empleados' => '42',
        ]);

        DB::table('empresas')->insert([
            'user_id' => 8,
            'nombre_s' => 'Empresa 3',
            'id_fiscal' => 'Q00000000',
            'dir_social' => 'Dirección Social de pruebas 4',
            'cnae_iae' => 'No se cuenta con un servicio de CNAE',
            'n_empleados' => '96',
        ]);
    }
}
