<?php

use Illuminate\Database\Seeder;

class OfertaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ofertas')->insert([
        	'empresa_id' => 1,
        	'centro_id' => 1,
        	'contacto_id' => 1,
        	'vacantes' => 15,
            'descripcion' => Str::random(100),
            'familia' => 'Tecnología y Comunicaciones',
            'perfil' => 'Desarrollo de Aplicaciones WEB, grado superior',
        ]);

        DB::table('ofertas')->insert([
            'empresa_id' => 1,
            'centro_id' => 2,
            'contacto_id' => 2,
            'vacantes' => 7,
            'descripcion' => Str::random(100),
            'familia' => 'Tecnología y Comunicaciones',
            'perfil' => 'Desarrollo de Aplicaciones Multiplataforma, grado superior',
        ]);

        DB::table('ofertas')->insert([
            'empresa_id' => 2,
            'centro_id' => 3,
            'contacto_id' => 3,
            'vacantes' => 7,
            'descripcion' => Str::random(100),
            'familia' => 'Administración y Finanzas',
            'perfil' => 'Gestión administrativa, grado medio',
        ]);
    }
}
