<?php

use Illuminate\Database\Seeder;

class InscripcionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('inscripciones')->insert([
        	'alumno_id' => 1,
        	'oferta_id' => 1,
        ]);

        DB::table('inscripciones')->insert([
        	'alumno_id' => 1,
        	'oferta_id' => 3,
        ]);

        DB::table('inscripciones')->insert([
        	'alumno_id' => 2,
        	'oferta_id' => 3,
        ]);

        DB::table('inscripciones')->insert([
        	'alumno_id' => 3,
        	'oferta_id' => 2,
        ]);
    }
}
